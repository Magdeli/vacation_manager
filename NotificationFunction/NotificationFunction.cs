﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using VacationManagerApp.dbContext;
using System.Linq;
using VacationManagerApp.shared;
using System.Collections.Generic;
using VacationManagerApp.Model;

namespace VacationManagerApp.NotificationFunction
{
    public class NotificationFunction
    {
        private readonly VacationManagerContext dbContext;

        public NotificationFunction(VacationManagerContext context)
        {
            dbContext = context;
        }

        [FunctionName("GetNotificationFunction")]
        public async Task<IActionResult> GetNotifications(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "notification")] HttpRequest req)
        {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            var notificationList = dbContext.Notifications.ToList();

            

            List<Notification> notificationsToReturn = notificationList.FindAll(
                notification => notification.UserId == authHelper.LoggedInUser.UserId
            );


            return new OkObjectResult(notificationsToReturn);
        }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Vacation_Manager_Models.Model;
using VacationManagerApp.dbContext;

namespace VacationManagerApp.shared {
    public class AuthHelper {

        private static string AuthUrl { get; set; }
        private string AuthToken { get; set; }

        private readonly VacationManagerContext dbContext;
        
        
        public User LoggedInUser { get; set; }
        public bool IsAuthenticated { get; set; }
        public bool IsAdministrator { get; set; }
        public string UserSub { get; set; }


        public override string ToString() {
            return $"IsAuthenticated = {IsAuthenticated}\nIsAdministrator = {IsAdministrator}\nLoggedInUser = {LoggedInUser}";
        }

        public AuthHelper(string authToken, VacationManagerContext dbContext) {
            this.dbContext = dbContext;

            AuthUrl = Environment.GetEnvironmentVariable("KeycloakServerUrl");
            
            // Remove the "Bearer: " part of string by getting the substring from index 8 (length of "Bearer: ")
            AuthToken = authToken.Replace( "Bearer ", string.Empty );

        }

        public static async Task<AuthHelper> InitializeHelperAsync(string authToken, VacationManagerContext dbContext) {

            AuthHelper authHelper = new AuthHelper(authToken, dbContext);

            var handler = new JwtSecurityTokenHandler();

            if (handler.CanReadToken(authHelper.AuthToken)) {

                authHelper.IsAuthenticated = await CheckIfAuthenticated(authHelper.AuthToken);

                var parsedUserToken = handler.ReadJwtToken(authHelper.AuthToken);
 
                authHelper.UserSub = parsedUserToken.Claims.First(claim => claim.Type == "sub").Value;

                authHelper.IsAdministrator = parsedUserToken.Claims.First(claim => claim.Type == "realm_access").Value.Contains("admin");

                authHelper.LoggedInUser = authHelper.FetchLoggedInUser(authHelper.UserSub);


                if(authHelper.LoggedInUser == null) {
                    authHelper.IsAuthenticated = false;
                }
            } 
            else {
                authHelper.IsAuthenticated = false;
            }

            return authHelper;


        }

        private static async Task<bool> CheckIfAuthenticated(string authToken) {

            if (authToken.Equals("") || authToken.Equals( null ) || authToken.Equals("null") ) {
                return false;      
            }

            //Console.WriteLine("TokenParsedType: " + parsedUserToken.GetType());

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Authorization", authToken);

            //Console.WriteLine($"{AuthUrl}/realms/experis-it/protocol/openid-connect/userinfo");

            /*
            // https://${keycloakHost}:${keycloakPort}/auth/realms/${realmName}/protocol/openid-connect/userinfo
            var isAuthenticated = await httpClient.GetAsync(
                    $"{AuthUrl}/realms/experis-it/protocol/openid-connect/auth" );

            
            Console.WriteLine("IsAuthenticated: " + isAuthenticated);

            Console.WriteLine("Type of isAuthenticated: " + isAuthenticated.GetType());
            */
            return true;

        }


        private User FetchLoggedInUser(string userSub) {


            var userList = this.dbContext.Users.ToList();

            User loggedInUser = null;

            try {
                loggedInUser = userList.Find(user => user.UserSub.Equals(userSub));
            }

            catch( Exception e ) {
                Console.WriteLine("Couldn't find user: " + e);
            }

            

            return loggedInUser;
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VacationManagerApp.dbContext;

namespace VacationManagerApp.shared {
    class DatabaseHelpers {

        public static async Task<IActionResult> SaveDbChangesAsync(ActionResult successResult, VacationManagerContext dbContext) {
            try {
                await dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException dbuce) {
                return new BadRequestObjectResult(
                    new ErrorResponse(400, "Concurrency issue when doing database operation", dbuce));
            }
            catch (DbUpdateException dbue) {
                return new BadRequestObjectResult(
                    new ErrorResponse(400, "Something went wrong when doing database operation", dbue));
            }

            return successResult;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VacationManagerApp.shared {
    class ErrorResponse {
        public string Message { get; }
        public int Code { get; }
        public Exception Exception { get; }

        public ErrorResponse(int code, string message, Exception exception) {
            Message = message;
            Code = code;
            Exception = exception;
        }
    }
}

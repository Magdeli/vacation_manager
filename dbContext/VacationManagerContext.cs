﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Vacation_Manager_Models.Model;
using VacationManagerApp.Model;

namespace VacationManagerApp.dbContext
{
    public class VacationManagerContext : DbContext
    {
        public VacationManagerContext(DbContextOptions<VacationManagerContext> options) : base(options) { }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<IneligiblePeriod> IneligiblePeriods { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<VacationRequestStatus> VacationRequestStatuses { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<NotificationType> NotificationTypes { get; set; }

    }


    public class VacationManagerContextFactory : IDesignTimeDbContextFactory<VacationManagerContext>
    {
        public VacationManagerContext CreateDbContext(string[] args)
        {
            string SqlConnection = Environment.GetEnvironmentVariable("ConnectionStrings:VacationManagerDb", EnvironmentVariableTarget.Process);

           
            var optionsBuilder = new DbContextOptionsBuilder<VacationManagerContext>();
            optionsBuilder.UseSqlServer(SqlConnection);

            return new VacationManagerContext(optionsBuilder.Options);
        }
    }
}

# Vacation Manager

The final project of the Experis Academy program. The project is casework for the customer Tidsbanken AS, and revolves around creating a system for creation and handling of vacation requests. 


## Getting started

This project consists of three parts:
- Frontend application built with [ReactJS](https://reactjs.org/) and [TypeScript](https://www.typescriptlang.org/)
- Backend API built with [Azure Functions](https://azure.microsoft.com/en-us/services/functions/?&ef_id=CjwKCAjwo9rtBRAdEiwA_WXcFtNpI1GREsRabzMYvKhuYy7Nx49YD4gyD5MIOPCYW24MahYFFLSsbhoC3xUQAvD_BwE:G:s&OCID=AID2000103_SEM_olsCz84L&MarinID=olsCz84L_326138898881_azure%20functions_e_c__62899293789_kwd-308142478483&lnkd=Google_Azure_Brand&dclid=CLiz7te_vuUCFcYBGQodQUIFEA)
- Standalone [Keycloak](https://www.keycloak.org/) server running as a [Docker image](https://hub.docker.com/r/jboss/keycloak/) in Azure App Service

This repository contains both the API and the website. The plan was to serve the webpage via the Functions Application, but there was not enough time to get this working properly.


## Installation:
To get this project up and running locally a few steps must be taken. Let's start with backend.


### Backend

The backend consists of a Azure Functions Application that servers as the API, as well as a standalone Keycloak server.


#### Keycloak

To setup the Keycloak server the docker image needs to be pulled from [Docker Hub - jboss/keycloak](https://hub.docker.com/r/jboss/keycloak/).

The setup is pretty straight forward and the link above provides a good walkthrough for how to set it up.

One thing to be aware of when the docker image is uploaded to Azure; communication between the App Service Application and the Docker image is HTTP based. This results in a error message when trying to access the admin panel. There might be a way to change this to HTTPS, but this has not been found by the authors. 

As a workaround for testing and development it is possible to turn of SSL in the Keycloak server. This is **NOT** recommended for production builds.

To turn of SSL:
1. Go to Realm settings in the admin panel
2. Switch to the Login tab pane
3. Require SSL should be set to none.

Keycloak can also be setup to support User Federation, which is authentication through an external user database. Out of the box Keycloak supports Active Directory and LDAP.

#### Azure Functions

To setup the API the only thing that needs to be done is creating a `local.setting.json` file in the root folder of the project. 

It's content should be as follows:
```
{
  "IsEncrypted": false,
  "Values": {
    "AzureWebJobsStorage": <your_azure_web_jobs_storage>,
    "FUNCTIONS_WORKER_RUNTIME": "dotnet",
    "KeycloakServerUrl": "<url_of_keycloak_server>/auth"
  },
  "ConnectionStrings": {
    "VacationManagerDb": <connection_string_to_sql_db>
  }

  // This part is for testing locally
  "Host": {
    "LocalHttpPort": 7071,
    "CORS": "*",
    "CORSCredentials": false
  },
}
```

It can be published directly to Azure from Visual Studio by right clicking the project folder followed by the *Publish* button.


### Frontend
To run the frontend locally navigate to `path/to/project/ClientApp` and run the install dependencies

```npm install```

Run a local version of the website:

```npm start```

This should start a local node server that is listening on port 3000 and can be reached at `http://localhost:3000/`

There also needs to be a `keycloak.json` file in the `/public` folder of the React project. It's contents should be the following:

```
{
	"realm": <keycloak_realm>,
	"auth-server-url": "<url_of_keycloak_server>/auth",
	"ssl-required": "none",
	"resource": <keycloak_client_name_id>,
	"public-client": true,
	"confidential-port": 0
}

```

## Authors
- **Håkon Råen** - Frontend ([Haakon94 @ GitLab](https://gitlab.com/Haakon94))
- **Mikail Andreassen** - Frontend ([Mikkor14 @ GitLab](
https://gitlab.com/mikkor14))
- **Odd Martin Hansen** - Backend / login ([Kegulf @ GitLab](https://www.gitlab.com/kegulf))
- **Magdeli Holmøy Asplin** - Backend / API ([Magdeli @ GitLab](https://gitlab.com/Magdeli))

## Technologies

#### Backend
- Docker
- Keycloak
- C# / .Net Core
- Azure Functions
- SQL Database

#### Frontend
- ReactJS
- TypeScript
- keycloak-js
- [react-keycloak]()
  
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VacationManagerApp.Model
{
    public class NotificationType
    {
        public int NotificationTypeId { get; set; }
        public string Type { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Vacation_Manager_Models.Model;

namespace VacationManagerApp.Model
{
    public class Notification
    {
        public int NotificationId { get; set; }
        public NotificationType NotificationType { get; set; }
        public int NotificationTypeId { get; set; }
        public string Message { get; set; }
        [NotMapped]
        public User User { get; set; }
        public int UserId { get; set; }
        [NotMapped]
        public Request Request { get; set; }
        public int RequestId { get; set; }
        public string TimeStamp { get; set; }
        public bool IsRead { get; set; }
        public User NotifierUser { get; set; }
        public int NotifierUserId { get; set; }
    }
}

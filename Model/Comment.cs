﻿using System.ComponentModel.DataAnnotations.Schema;


namespace Vacation_Manager_Models.Model
{
    // Vacation Manager Case
    // 28/10/2019

    // This is the comment class used to create comments

    public class Comment
    {
        public int CommentId { get; set; }
        public string Message { get; set; }
        [NotMapped]
        public Request Request { get; set; }
        public int RequestId { get; set; }
        [NotMapped]
        public User User { get; set; }
        public int UserId { get; set; }
        public string CreatedAt { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Vacation_Manager_Models.Model
{
    // Vacation Manager Case
    // 28/10/2019

    // This is the request class which is used to create requests

    public class Request
    {
        public int RequestId { get; set; }
        [NotMapped]
        public User User { get; set; }
        public int UserId { get; set; }
        [NotMapped]
        public VacationRequestStatus VacationRequestStatus { get; set; }
        public int VacationRequestStatusId { get; set; }
        public string Title { get; set; }
        public string PeriodStart { get; set; }
        public string PeriodEnd { get; set; }
        [NotMapped]
        public User Admin { get; set; }
        public int AdminId { get; set; }

        public string ModifiedAt { get; set; }
    }
}

﻿
namespace Vacation_Manager_Models.Model
{
    // Vacation Manager Case
    // 28/10/2019

    // This is the vacation request status class which is used to create three vacation request statuses when the program initially run

    public class VacationRequestStatus
    {
        public int VacationRequestStatusId { get; set; }
        public string Status { get; set; }

    }
}

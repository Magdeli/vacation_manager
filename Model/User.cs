﻿
namespace Vacation_Manager_Models.Model
{
    // Vacation Manager Case
    // 28/10/2019

    // This is the user class which is used to create a user

    public class User
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string ProfilePicture { get; set; }
        public bool IsAdmin { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserSub { get; set; }

        public override string ToString() {
            return $"{UserId}: {FirstName} {LastName}";
        }
    }
}

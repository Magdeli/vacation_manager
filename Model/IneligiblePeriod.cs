﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Vacation_Manager_Models.Model
{
    // Vacation Manager Case
    // 28/10/2019

    // This is the ineligible period class which is used to create ineligible periods

    public class IneligiblePeriod
    {
        public int IneligiblePeriodId { get; set; }
        public string PeriodStart { get; set; }
        public string PeriodEnd { get; set; }
        [NotMapped]
        public User Admin { get; set; }
        public int AdminId { get; set; }
    }
}

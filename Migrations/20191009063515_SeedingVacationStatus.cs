﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VacationManagerApp.Migrations
{
    public partial class SeedingVacationStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO VacationRequestStatuses (Status) VALUES ('Pending')");
            migrationBuilder.Sql("INSERT INTO VacationRequestStatuses (Status) VALUES ('Approved')");
            migrationBuilder.Sql("INSERT INTO VacationRequestStatuses (Status) VALUES ('Denied')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM VacationRequestStatuses WHERE Status='Pending'");
            migrationBuilder.Sql("DELETE FROM VacationRequestStatuses WHERE Status='Approved'");
            migrationBuilder.Sql("DELETE FROM VacationRequestStatuses WHERE Status='Denied'");
        }
    }
}

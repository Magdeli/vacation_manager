﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using VacationManagerApp.dbContext;

namespace VacationManagerApp.Migrations
{
    [DbContext(typeof(VacationManagerContext))]
    [Migration("20191009063515_SeedingVacationStatus")]
    partial class SeedingVacationStatus
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Vacation_Manager_Models.Model.Comment", b =>
                {
                    b.Property<int>("CommentId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CreatedAt");

                    b.Property<string>("Message");

                    b.Property<int>("RequestId");

                    b.Property<int>("UserId");

                    b.HasKey("CommentId");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("Vacation_Manager_Models.Model.IneligiblePeriod", b =>
                {
                    b.Property<int>("IneligiblePeriodId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AdminId");

                    b.Property<string>("PeriodEnd");

                    b.Property<string>("PeriodStart");

                    b.HasKey("IneligiblePeriodId");

                    b.ToTable("IneligiblePeriods");
                });

            modelBuilder.Entity("Vacation_Manager_Models.Model.Request", b =>
                {
                    b.Property<int>("RequestId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AdminId");

                    b.Property<string>("PeriodEnd");

                    b.Property<string>("PeriodStart");

                    b.Property<string>("Title");

                    b.Property<int>("UserId");

                    b.Property<int>("VacationRequestStatusId");

                    b.HasKey("RequestId");

                    b.ToTable("Requests");
                });

            modelBuilder.Entity("Vacation_Manager_Models.Model.User", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<bool>("IsAdmin");

                    b.Property<string>("LastName");

                    b.Property<string>("ProfilePicture");

                    b.HasKey("UserId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Vacation_Manager_Models.Model.VacationRequestStatus", b =>
                {
                    b.Property<int>("VacationRequestStatusId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Status");

                    b.HasKey("VacationRequestStatusId");

                    b.ToTable("VacationRequestStatuses");
                });
#pragma warning restore 612, 618
        }
    }
}

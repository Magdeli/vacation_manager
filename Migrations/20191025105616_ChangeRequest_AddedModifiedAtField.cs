﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VacationManagerApp.Migrations
{
    public partial class ChangeRequest_AddedModifiedAtField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ModifiedAt",
                table: "Requests",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ModifiedAt",
                table: "Requests");
        }
    }
}

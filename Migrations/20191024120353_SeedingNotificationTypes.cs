﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VacationManagerApp.Migrations
{
    public partial class SeedingNotificationTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO NotificationTypes (Type) VALUES ('has made a comment on your request.')");
            migrationBuilder.Sql("INSERT INTO NotificationTypes (Type) VALUES ('has approved your vacation request.')");
            migrationBuilder.Sql("INSERT INTO NotificationTypes (Type) VALUES ('has denied your vacation request.')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM NotificationTypes WHERE Type='has made a comment on your request.'");
            migrationBuilder.Sql("DELETE FROM NotificationTypes WHERE Type='has approved your vacation request.'");
            migrationBuilder.Sql("DELETE FROM NotificationTypes WHERE Type='has denied your vacation request.'");
        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Vacation_Manager_Models.Model;
using VacationManagerApp.dbContext;
using VacationManagerApp.Model;
using VacationManagerApp.shared;

namespace VacationManagerApp.RequestFunction
{
    // Vacation Manager Case
    // 28/10/2019

    // This is the request controller which holds the endpoints for GET/, GET/byId, POST/, PATCH/, DELETE/, GET/comments, 
    // GET/commentById, POST/comment, PATCH/comment and DELETE/comment

    public class RequestFunction
    {
        private readonly VacationManagerContext dbContext;

        public RequestFunction(VacationManagerContext context)
        {
            dbContext = context;
        }




        [FunctionName("GetRequestFunction")]
        // This function returns all requests visible to the current user
        public async Task<IActionResult> GetRequests(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "request/")] HttpRequest req)
        {
            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            // List of all the vacation requests
            var allVacationRequests = dbContext.Requests.ToList().OrderBy(request => request.PeriodStart).ToList();

            // Administrators are allowed to see all requests
            if (authHelper.IsAdministrator) { 
                return new OkObjectResult(allVacationRequests);
            }

            else {
                int userId = authHelper.LoggedInUser.UserId;

                // A user gets all of its own request, as well as all approved requests
                List<Request> requestsToReturn = allVacationRequests.FindAll(
                    request => request.UserId == userId || request.VacationRequestStatusId == 2
                );

                return new OkObjectResult(requestsToReturn);
            }
        }

        


        [FunctionName("CreateRequestFunction")]
        // This function creates a new request within the system
        public async Task<IActionResult> CreateRequests(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "request/")] HttpRequest req)
        {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated)
            {
                return new UnauthorizedResult();
            }

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var vacationRequest = JsonConvert.DeserializeObject<Request>(requestBody);

            // Setting the initial vacation request status to be pending
            vacationRequest.VacationRequestStatusId = 1;

            // Adding the request to the database
            dbContext.Requests.Add(vacationRequest);

            return await DatabaseHelpers.SaveDbChangesAsync(new CreatedResult("", vacationRequest), dbContext);
        }

        

        
        [FunctionName("GetRequestByIdFunction")]
        // This function returns the request of this id
        public async Task<IActionResult> GetRequestById(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "request/{request_id}")] HttpRequest req, int request_id)
        {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            // Get the request of this id
            var vacationRequest = dbContext.Requests.Find(request_id);

            User requestingUser = dbContext.Users.Find(vacationRequest.UserId);


            // Only admin and self can view requests that are not approved
            if ( ! authHelper.IsAdministrator && authHelper.LoggedInUser.UserId != vacationRequest.UserId && vacationRequest.VacationRequestStatusId != 2) {
                return new ForbidResult();
            }    

            return new OkObjectResult(new {
                User = requestingUser,
                VacationRequest = vacationRequest
            });
        }




        [FunctionName("UpdateRequestFunction")]
        // This function updates a request
        public async Task<IActionResult> UpdateRequest(
            [HttpTrigger(AuthorizationLevel.Anonymous, "patch", Route = "request/{request_id}")] HttpRequest req, int request_id)
        {
            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            var requestToUpdate = dbContext.Requests.Find(request_id);

            // Only admin can update a request after it is approved
            if (requestToUpdate.VacationRequestStatusId != 1 && !authHelper.IsAdministrator)
            {
                return new ForbidResult();
            }
            
               
            // Only Admin and self can update a request.
            if( ! authHelper.IsAdministrator && requestToUpdate.UserId != authHelper.LoggedInUser.UserId ) {
                return new ForbidResult();
            }
            

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            Request partialRequest = JsonConvert.DeserializeObject<Request>(requestBody);

            // Admins can update its requests, but not the status. (You cant approve your own vacation)
            if( authHelper.IsAdministrator && requestToUpdate.UserId != authHelper.LoggedInUser.UserId ) {


                if (partialRequest.VacationRequestStatusId != requestToUpdate.VacationRequestStatusId ) {

                    requestToUpdate.VacationRequestStatusId = partialRequest.VacationRequestStatusId;
                    
                    Notification notification = new Notification();
                    notification.NotifierUserId = authHelper.LoggedInUser.UserId;
                    notification.UserId = requestToUpdate.UserId;
                    notification.TimeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");

                    notification.Message = $"<b>{authHelper.LoggedInUser.FirstName} {authHelper.LoggedInUser.LastName}</b> ";

                    // If the status is "approved"
                    if (partialRequest.VacationRequestStatusId == 2)
                    {
                        notification.Message += $"approved your request <b>{requestToUpdate.Title}</b>";
                        notification.NotificationTypeId = 2;
                    }
                    else // If the status is "denied"
                    {
                        notification.Message += $"rejected your request <b>{requestToUpdate.Title}</b>";
                        notification.NotificationTypeId = 3;
                    }
                    dbContext.Notifications.Add(notification);
                }
            }

            // Only users can update their own requests.
            if( requestToUpdate.UserId == authHelper.LoggedInUser.UserId ) {
                if (partialRequest.Title != null && partialRequest.Title != requestToUpdate.Title) {
                    requestToUpdate.Title = partialRequest.Title;
                }

                // Update the period start
                if (partialRequest.PeriodStart != null && partialRequest.PeriodStart != requestToUpdate.PeriodStart) {
                    requestToUpdate.PeriodStart = partialRequest.PeriodStart;
                }

                // Update the period end
                if (partialRequest.PeriodEnd != null && partialRequest.PeriodEnd != requestToUpdate.PeriodEnd) {
                    requestToUpdate.PeriodEnd = partialRequest.PeriodEnd;
                }
            }


            return await DatabaseHelpers.SaveDbChangesAsync( new OkObjectResult(requestToUpdate), dbContext );
        }




        [FunctionName("DeleteRequestFunction")]
        // This function deletes a request
        public async Task<IActionResult> DeleteRequest(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "request/{request_id}")] HttpRequest req, int request_id)
        {
            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            // Only administrators can delete a request
            if ( ! authHelper.IsAdministrator ) {
                return new ForbidResult();
            }

            // Find the request and delete it from the dbContext
            var request = await dbContext.Requests.FindAsync(request_id);
            dbContext.Requests.Remove(request);

            // Find all comments for the rerquest and delete them if there are any.
            var commentList = dbContext.Comments.ToList();
            
            if( commentList.Count != 0 ) { 
                commentList.RemoveAll(comment => comment.RequestId == request_id);
            }

            return await DatabaseHelpers.SaveDbChangesAsync(new NoContentResult(), dbContext);
        }




        [FunctionName("GetCommentsFunction")]
        // This function gets all the comments of a request
        public async Task<IActionResult> GetComments(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "request/{request_id}/comments")] HttpRequest req, int request_id)
        {
            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            // Find the request so we can use the userId later.
            Request request = await dbContext.Requests.FindAsync(request_id);

            // If the request wasn't found, send a 404 back
            if( request == null ) {
                return new NotFoundObjectResult(
                    new ErrorResponse(404, "Could not find the vacation request in question", null));
            }


            
            // Only admin and the user that made the request is able to get the comments
            if( ! authHelper.IsAdministrator && authHelper.LoggedInUser.UserId != request.UserId) {
                return new ForbidResult();
            }

            

            var allComments = dbContext.Comments.ToList();
            var allUsers = dbContext.Users.ToList();

            var commentsToReturn = 
               from comment in allComments
               join user in allUsers
               on comment.UserId equals user.UserId
               where comment.RequestId == request_id
               select new {
                   Comment = comment,
                   User = new { Name = user.FirstName + " " + user.LastName, ProfilePicture = user.ProfilePicture }
               };

            // Comments should be returned in reverse chronological order
            commentsToReturn.Reverse();

            return new OkObjectResult(commentsToReturn);
        }




        [FunctionName("AddCommentFunction")]
        // This function creates a new comment
        public async Task<IActionResult> AddComment(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "request/{request_id}/comments")] HttpRequest req, int request_id)
        {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            // Find the request so we can use the userId later.
            Request request = await dbContext.Requests.FindAsync(request_id);

            // If the request wasn't found, send a 404 back
            if (request == null) {
                return new NotFoundObjectResult(
                    new ErrorResponse(404, "Could not find the vacation request in question", null));
            }


            
            // Only admin and the user that made the request is able to add comments to a request
            if (!authHelper.IsAdministrator && authHelper.LoggedInUser.UserId != request.UserId) {
                return new ForbidResult();
            }

            

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var comment = JsonConvert.DeserializeObject<Comment>(requestBody);
            dbContext.Comments.Add(comment);

            CreateCommentNotifications(request, authHelper.LoggedInUser);

            return await DatabaseHelpers.SaveDbChangesAsync(new OkObjectResult(comment), dbContext);
        }


        private void CreateCommentNotifications(Request request, User loggedInUser) {


            List<Comment> requestComments = dbContext.Comments.ToList().FindAll(comment => comment.RequestId == request.RequestId);

            List<int> usersToNotify = new List<int>();

            foreach(Comment c in requestComments) {
                if(! usersToNotify.Contains(c.UserId) && loggedInUser.UserId != c.UserId) {
                    usersToNotify.Add(c.UserId);
                }
            }

            foreach(int userToNotify in usersToNotify) {

                Notification notification = new Notification();

                notification.NotifierUserId = loggedInUser.UserId;
                notification.UserId = userToNotify;
                notification.IsRead = false;
                notification.NotificationTypeId = 1;
                notification.TimeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
                notification.Message = $"<b>{loggedInUser.FirstName} {loggedInUser.LastName}</b> commented on the request <b>{request.Title}</b>";

                dbContext.Notifications.Add(notification);
            }
        }
        
        
        [FunctionName("GetCommentByIdFunction")]
        // This function gets the comment of this id
        public async Task<IActionResult> GetCommentById(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "request/{request_id}/comments/{comment_id}")] HttpRequest req, 
            int request_id,
            int comment_id)
        {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            // Find the request so we can use the userId later.
            Request request = await dbContext.Requests.FindAsync(request_id);

            // If the request wasn't found, send a 404 back
            if (request == null) {
                return new NotFoundObjectResult(
                    new ErrorResponse(404, "Could not find the vacation request in question", null));
            }

            
            // Only admin and the user that made the request is able to get its comments
            if (!authHelper.IsAdministrator && authHelper.LoggedInUser.UserId != request.UserId) {
                return new ForbidResult();
            }
            

            var comment = dbContext.Comments.Find(comment_id);

            if( comment == null ) {
                return new NotFoundObjectResult(
                    new ErrorResponse(404, "Could not find the specified comment", null));
            }

            return new OkObjectResult(comment);
        }




        [FunctionName("UpdateCommentFunction")]
        // This function updates a comment
        public async Task<IActionResult> UpdateComment(
            [HttpTrigger(AuthorizationLevel.Anonymous, "patch", Route = "request/{request_id}/comments/{comment_id}")] HttpRequest req, 
            int request_id, 
            int comment_id)
        {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            // Find the request so we can use the userId later.
            Comment comment = await dbContext.Comments.FindAsync(comment_id);

            // If the request wasn't found, send a 404 back
            if (comment == null) {
                return new NotFoundObjectResult(
                    new ErrorResponse(404, "Could not find the specified comment", null));
            }

            

            // Only the user that made the comment is able to edit it 
            if ( authHelper.LoggedInUser.UserId != comment.UserId ) {
                return new ForbidResult();
            }

            

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var partialComment = JsonConvert.DeserializeObject<Comment>(requestBody);
            var commentToUpdate = dbContext.Comments.Find(comment_id);

            // Update the comment message
            if (partialComment.Message != null && partialComment.Message != commentToUpdate.Message)
            {
                commentToUpdate.Message = partialComment.Message;
            }

            return await DatabaseHelpers.SaveDbChangesAsync(new OkObjectResult(commentToUpdate), dbContext);
        }




        [FunctionName("DeleteCommentFunction")]
        // This function deletes a comment
        public async Task<IActionResult> DeleteComment(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "request/{request_id}/comments/{comment_id}")] HttpRequest req, 
            int request_id,
            int comment_id)
        {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            // Find the request so we can use the userId later.
            Comment comment = await dbContext.Comments.FindAsync(comment_id);

            // If the comment wasn't found, send a 404 back
            if (comment == null) {
                return new NotFoundObjectResult(
                    new ErrorResponse(404, "Could not find the specified comment", null));
            }

            // Only the user that made the comment is able to delete it 
            if (!authHelper.IsAdministrator && authHelper.LoggedInUser.UserId != comment.UserId) {
                return new ForbidResult();
            }


            dbContext.Comments.Remove(comment);
            dbContext.SaveChanges();

            return new NoContentResult();
        }
    }
}

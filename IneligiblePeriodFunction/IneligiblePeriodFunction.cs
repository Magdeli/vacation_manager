﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
using VacationManagerApp.dbContext;
using Vacation_Manager_Models.Model;
using VacationManagerApp.shared;

namespace VacationManagerApp.IneligiblePeriodFunction
{
    // Vacation Manager Case
    // 28/10/2019

    // This is the ineligible period controller which holds the endpoints for GET/, POST/, GET/byId, PATCH/ and DELETE/

    public class IneligiblePeriodFunction
    {

        private readonly VacationManagerContext dbContext;

        public IneligiblePeriodFunction (VacationManagerContext context)
        {
            dbContext = context;
        }




        [FunctionName("GetIneligiblePeriodFunction")]
        // This function returns all ineligible periods
        public async Task<IActionResult> GetIneligiblePeriods(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "ineligible")] HttpRequest req)
        {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            return new OkObjectResult(dbContext.IneligiblePeriods.ToList());
            
        }




        [FunctionName("AddIneligiblePeriodFunction")]
        // This function creates a new ineligible period
        public async Task<IActionResult> AddIneligiblePeriod(
           [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "ineligible")] HttpRequest req)
        {
            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);


            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            if ( ! authHelper.IsAdministrator )
            {
                return new ForbidResult();
            }

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            
            IneligiblePeriod period = JsonConvert.DeserializeObject<IneligiblePeriod>(requestBody);
            dbContext.IneligiblePeriods.Add(period);
            
            return await DatabaseHelpers.SaveDbChangesAsync(new OkObjectResult(period), dbContext);
        }




        [FunctionName("GetIneligiblePeriodByIdFunction")]
        // This function returns the ineligible period of this id 
        public async Task<IActionResult> GetIneligiblePeriodById(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "ineligible/{ip_id}")] HttpRequest req, 
            int ip_id)
        {
            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            var period = dbContext.IneligiblePeriods.Find(ip_id);

            if( period == null ) {
                return new NotFoundObjectResult(
                    new ErrorResponse(404, "Could not find the ineligible period in question", null));
            }

            return new OkObjectResult(period);
        }




        [FunctionName("UpdateIneligiblePeriodFunction")]
        // This function updates an ineligible period
        public async Task<IActionResult> UpdateIneligiblePeriod(
            [HttpTrigger(AuthorizationLevel.Anonymous, "patch", Route = "ineligible/{ip_id}")] HttpRequest req, 
            int ip_id)
        {
            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            if (!authHelper.IsAdministrator)
            {
                return new ForbidResult();
            }

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var partialPeriod = JsonConvert.DeserializeObject<IneligiblePeriod>(requestBody);
            
            var periodToUpdate = dbContext.IneligiblePeriods.Find(ip_id);

            if (partialPeriod.PeriodStart != null && partialPeriod.PeriodStart != periodToUpdate.PeriodStart)
            {
                periodToUpdate.PeriodStart = partialPeriod.PeriodStart;
            }

            if (partialPeriod.PeriodEnd != null && partialPeriod.PeriodEnd != periodToUpdate.PeriodEnd)
            {
                periodToUpdate.PeriodEnd = partialPeriod.PeriodEnd;
            }

            return await DatabaseHelpers.SaveDbChangesAsync(new OkObjectResult(periodToUpdate), dbContext);
        }




        [FunctionName("DeleteIneligiblePeriodFunction")]
        // This function deletes an ineligible period
        public async Task<IActionResult> DeleteIneligiblePeriod(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "ineligible/{ip_id}")] HttpRequest req, 
            int ip_id)
        {
            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            if (!authHelper.IsAdministrator)
            {
                return new ForbidResult();
            }

            var period = dbContext.IneligiblePeriods.Find(ip_id);
            dbContext.IneligiblePeriods.Remove(period);

            return await DatabaseHelpers.SaveDbChangesAsync(new NoContentResult(), dbContext);
        }
    }
}

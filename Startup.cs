﻿using System;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using VacationManagerApp.dbContext;

[assembly: FunctionsStartup(typeof(VacationManagerApp.Startup))]

namespace VacationManagerApp
{
    class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder) 
        {    

            // Get the DB string from local.settings.json
            string conStr = Environment.GetEnvironmentVariable(
                    $"ConnectionStrings:VacationManagerDb", EnvironmentVariableTarget.Process);
            
            // If the conStr is empty we know we're not in localhost, get the 
            // connection string from the Env vars in Azure App Settings
            if (string.IsNullOrEmpty(conStr)) // Azure Functions App Service naming convention
                conStr = Environment.GetEnvironmentVariable(
                    $"SQLAZURECONNSTR_VacationManagerDb", EnvironmentVariableTarget.Process);


            // COMMON TO BOTH VERSIONS
            builder.Services.AddDbContext<VacationManagerContext>(
                //options => options.UseSqlServer(Environment.GetEnvironmentVariable("VacationManagerDb")));
                options => options.UseSqlServer(conStr));

        }
    }
}

using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using VacationManagerApp.dbContext;
using System.Linq;
using Vacation_Manager_Models.Model;
using Microsoft.EntityFrameworkCore;
using VacationManagerApp.shared;
using System.Collections.Generic;
using System;

namespace VacationManagerApp
{
    // Vacation Manager Case
    // 28/10/2019

    // This is the user controller which holds the endpoint for GET/, GET/loggedIn, GET/byId, POST/, PATCH/, DELETE/, 
    // GET/requests and POST/updatePassword

    public class UserFunction
    {

        private readonly VacationManagerContext dbContext;

        public UserFunction(VacationManagerContext context)
        {
            dbContext = context;
        }


        [FunctionName("UserFunction")]
        // This function returns the current logged in user
        public async Task<IActionResult> GetLoggedInUser(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "user/")] HttpRequest req)
        {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if ( ! authHelper.IsAuthenticated )
            {
                return new UnauthorizedResult();
            }


            return new OkObjectResult(authHelper.LoggedInUser);
        }





        [FunctionName("GetAllUsersFunction")]
        // This function returns all users within the system
        public async Task<IActionResult> GetAllUsers(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "user/all")] HttpRequest req) 
        {
            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if ( ! authHelper.IsAuthenticated ) {
                return new UnauthorizedResult();
            }

            // Only administrators can get all users
            if ( ! authHelper.IsAdministrator ) {
                return new ForbidResult();
            }

            // List of all users
            var userList = await dbContext.Users.ToListAsync();

            return new OkObjectResult(userList);
            
        }





        [FunctionName("GetUserByIdFunction")]
        // This function returns the user with this id
        public async Task<IActionResult> GetUserById(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "user/{user_id}")] HttpRequest req,
            int user_id) {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if ( ! authHelper.IsAuthenticated ) {
                return new UnauthorizedResult();
            }

            // The user of this id
            User userToFetch = dbContext.Users.Find(user_id);

            // Check if there exists a user of this id
            if (userToFetch == null) {
                return new NoContentResult();
            }


            // The user and admins should get all info about the user
            if (authHelper.IsAdministrator || authHelper.LoggedInUser.Equals(userToFetch)) {
                return new OkObjectResult(userToFetch);
            }

            // Other users should only get the name and profile picture
            else {
                return new OkObjectResult(new {
                    userToFetch.FirstName,
                    userToFetch.LastName,
                    userToFetch.ProfilePicture
                });
            };

        }




        [FunctionName("CreateUserFunction")]
        // This function creates a new user within the system
        public async Task<IActionResult> CreateUser(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "user/")] HttpRequest req)
        {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            // Only administrators can create new users
            if (!authHelper.IsAdministrator) {
                return new ForbidResult();
            }

            // Read the body of the request
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();

            // Deserialize the body into an User object and add it to the db
            var userToAdd = JsonConvert.DeserializeObject<User>(requestBody);
            
            // Adding the user to the database
            dbContext.Users.Add(userToAdd);

            return await DatabaseHelpers.SaveDbChangesAsync( new CreatedResult("", userToAdd), dbContext );            
        }





        [FunctionName("DeleteUserFunction")]
        // This function deletes a user
        public async Task<IActionResult> DeleteUser(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "user/{user_id}")] HttpRequest req,
            int user_id)
        {
            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            // Find the user of this id
            User userToDelete = dbContext.Users.Find(user_id);

            // Check if there exists a user of this id
            if (userToDelete == null)
            {
                return new NoContentResult();
            }

            // Only admin and self can delete a user
            if (!authHelper.IsAdministrator && !authHelper.LoggedInUser.Equals(userToDelete)) {
                return new ForbidResult();
            }

            // Removing the user from the database
            dbContext.Users.Remove(userToDelete);

            return await DatabaseHelpers.SaveDbChangesAsync(new NoContentResult(), dbContext);
        }



        [FunctionName("RequestUserFunction")]
        // This function returns the requests of the user of this id
        public async Task<IActionResult> GetUserRequests(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "user/{user_id}/requests")] HttpRequest req,
           int user_id) {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            // A list of all the requests of the user of this user id
            List<Request> userRequests = dbContext.Requests.ToList().FindAll(
               request => request.UserId == user_id
           );


            // Only admin and self
            if (!authHelper.IsAdministrator && !authHelper.LoggedInUser.UserId.Equals(user_id)) {
                return new OkObjectResult(
                    userRequests.FindAll(request => request.VacationRequestStatusId == 2).ToList());
            }

           

            return new OkObjectResult(userRequests);
        }



        [FunctionName("UpdateUserFunction")]
        // This function updates one or more attributes of the user of this id
        public async Task<IActionResult> UpdateUser(
            [HttpTrigger(AuthorizationLevel.Anonymous, "patch", Route = "user/{user_id}")] HttpRequest req,
            int user_id)     
        {

            AuthHelper authHelper = await AuthHelper.InitializeHelperAsync(req.Headers["Authorization"], dbContext);

            // Only available after logging in
            if (!authHelper.IsAuthenticated) {
                return new UnauthorizedResult();
            }

            // Only admin and self can update a user
            if (!authHelper.IsAdministrator && !authHelper.LoggedInUser.UserId.Equals(user_id)) {
                return new ForbidResult();
            }

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var partialUser = JsonConvert.DeserializeObject<User>(requestBody);

            var userToUpdate = await dbContext.Users.FindAsync(user_id);

            // Update first name
            if (partialUser.FirstName != null && partialUser.FirstName != userToUpdate.FirstName)
            {
                userToUpdate.FirstName = partialUser.FirstName;
            }

            // Update email
            if (partialUser.Email != null && partialUser.Email != userToUpdate.Email)
            {
                userToUpdate.Email = partialUser.Email;
            }

            // Update last name
            if (partialUser.LastName != null && partialUser.LastName != userToUpdate.LastName)
            {
                userToUpdate.LastName = partialUser.LastName;
            }

            // Update profile picture
            if (partialUser.ProfilePicture != null && partialUser.ProfilePicture != userToUpdate.ProfilePicture)
            {
                userToUpdate.ProfilePicture = partialUser.ProfilePicture;
            }

            // Update the admin property
            if (partialUser.IsAdmin != userToUpdate.IsAdmin) {
                userToUpdate.IsAdmin = partialUser.IsAdmin;
            }

            return await DatabaseHelpers.SaveDbChangesAsync(new OkObjectResult(userToUpdate), dbContext);
        }    



        [FunctionName("ChangePasswordFunction")]
        // This function updates the password of the user of this id
        public async Task<IActionResult> ChangePassword(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "user/{user_id}/update_password")] HttpRequest req,
            string user_id)
        {
            return new OkObjectResult($"Changed password of logged in user. User id: {user_id}");
        }
    }
}

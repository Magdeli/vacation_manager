import React, { createContext, useState } from "react";

// @ts-ignore
export const AuthContext = createContext();

const AuthContextProvider: React.FC = props => {
	const [tokens, setTokens] = useState({
		idToken: "",
		refreshToken: ""
	});

	const [currentUser, setCurrentUser] = useState(null);

	const [loading, setLoading] = useState(false);

	const addTokens = (tokens: any) => {
		setTokens(tokens);
	};

	const addCurrentUser = (user: any) => {
		setCurrentUser(user);
	};

	const removeTokens = () => {
		setTokens({ idToken: "", refreshToken: "" });
	};

	const removeCurrentUser = () => {
		setCurrentUser(null);
	};

	return (
		<AuthContext.Provider
			value={{
				tokens,
				addTokens,
				removeTokens,

				currentUser,
				addCurrentUser,
				removeCurrentUser,

				loading,
				setLoading
			}}
		>
			{props.children}
		</AuthContext.Provider>
	);
};

export default AuthContextProvider;

import React, { useContext, useState, useEffect } from "react";

import { BrowserRouter } from "react-router-dom";

import NavMenu from "./Components/NavMenu/NavMenu";

import {
	AdminSettings,
	CalenderDashboard,
	MyVacations,
	NewRequestForm,
	UserProfile,
	ViewRequestPage
} from "./Pages";

import "./App.css";
import AuthorizedRoute from "./Components/AuthorizedRoute/AuthorizedRoute";
import { KeycloakProvider, useKeycloak } from "react-keycloak";
import Keycloak from "keycloak-js";
import { Spinner } from "reactstrap";
import { getLoggedInUser } from "./dataFetcher";
import { AuthContext } from "./Contexts/AuthContext";

const keycloak: Keycloak.KeycloakInstance = Keycloak("/keycloak.json");

keycloak.onAuthSuccess = () => {};

const App: React.FC = () => {
	const { currentUser, addCurrentUser } = useContext(AuthContext);

	const [loading, setLoading] = useState(false);

	return (
		<KeycloakProvider
			keycloak={keycloak}
			initConfig={{ onLoad: "login-required", checkLoginIframe: false }}
			LoadingComponent={
				<div className="text-center">
					<Spinner />
				</div>
			}
			onToken={tokens => {
				setLoading(true);
				sessionStorage.setItem("react-token", keycloak.token || "");
				sessionStorage.setItem("react-refreshToken", keycloak.refreshToken || "");
				getLoggedInUser()
					.then(user => {
						addCurrentUser(user);
						setLoading(false);
					})
					.catch(error => {
						console.log(error);
						setLoading(false);
					});

				setTimeout(refreshAuthToken, 60000);
			}}
		>
			{loading && !currentUser ? (
				<div className="text-center">
					<Spinner />
				</div>
			) : (
				<div className="App">
					<BrowserRouter>
						<NavMenu />
						<AuthorizedRoute adminOnly={true} path={"/admin"} component={AdminSettings} />
						<AuthorizedRoute exact={true} path={"/"} component={CalenderDashboard} />
						<AuthorizedRoute path={"/profile"} component={UserProfile} />
						<AuthorizedRoute path={"/vacations/:id"} component={MyVacations} />
						<AuthorizedRoute path={"/newrequest"} component={NewRequestForm} />
						<AuthorizedRoute exact={true} path={"/vacation/:id"} component={ViewRequestPage} />
					</BrowserRouter>
				</div>
			)}
		</KeycloakProvider>
	);
};
const refreshAuthToken = () => {
	keycloak
		.updateToken(70)
		.success(refreshed => {
			if (refreshed) {
				console.debug("Token refreshed" + refreshed);
			} else {
				if (keycloak.tokenParsed && keycloak.tokenParsed.exp && keycloak.timeSkew)
					console.warn(
						"Token not refreshed, valid for " +
							Math.round(
								keycloak.tokenParsed.exp + keycloak.timeSkew - new Date().getTime() / 1000
							) +
							" seconds"
					);
				else {
					console.warn("Token not refreshed, still valid.");
				}
			}
		})
		.error(() => {
			console.error("Failed to refresh token");
		});
};

export default App;

import React, { useState } from "react";
import {
	Button,
	Form,
	FormGroup,
	Label,
	Input,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter
} from "reactstrap";

import { patchUser } from "../../dataFetcher";

type User = {
	userId: number;
	email: string;
	isAdmin: boolean;
	firstName: string;
	lastName: string;
	profilePicture: string;
};

const EditModal: React.FC<{ User: User; toggleModal: any; isOpen: boolean }> = ({
	User,
	toggleModal,
	isOpen
}) => {
	const [inputValues, setInputValues] = useState({});

	const handleModalSubmit = (event: any) => {
		event.preventDefault();
		console.log("PUT data for user here");
		sendData();
		setInputValues({});
	};

	const onInputChange = (e: React.FormEvent<HTMLInputElement>) => {
		const { name, value } = e.currentTarget;
		setInputValues({ ...inputValues, [name]: value });
	};

	const handleChangeType = (e: any) => {
		const { name, value } = e.currentTarget;
		setInputValues({ ...inputValues, [name]: value === "Admin" });
	};

	const sendData = () => {
		patchUser(inputValues, User.userId)
			.then(data => {
				alert(`User was successfully updated`);
			})
			.catch(error => {
				console.log(error);
				alert(`Error updating user! Check the error logs for more info!`);
			});
	};

	return (
		<div className="modal-div">
			<Modal isOpen={isOpen} toggle={toggleModal} className="edit-user-modal">
				<ModalHeader toggle={toggleModal}>Edit user details: </ModalHeader>
				<ModalBody>
					<Form className="admin-edit-user-form" onSubmit={handleModalSubmit}>
						<FormGroup>
							<Label>Username(Email):</Label>
							<Input
								className="form-control"
								type="text"
								name="email"
								defaultValue={User.email}
								onChange={onInputChange}
							/>
						</FormGroup>
						<FormGroup>
							<Label>First name:</Label>
							<Input
								className="form-control"
								type="text"
								name="firstName"
								defaultValue={User.firstName}
								onChange={onInputChange}
							/>
						</FormGroup>
						<FormGroup>
							<Label>Last name:</Label>
							<Input
								className="form-control"
								type="text"
								name="lastName"
								defaultValue={User.lastName}
								onChange={onInputChange}
							/>
						</FormGroup>
						<FormGroup>
							<label htmlFor="admin-new-user-form">Select type of user:</label>
							<select
								className="form-control"
								id="user-privilege-select"
								name="isAdmin"
								onChange={handleChangeType}
							>
								<option>User</option>
								<option>Admin</option>
							</select>
						</FormGroup>
						<Button type="submit" color="primary" value="Update user">
							Update user details
						</Button>
					</Form>
				</ModalBody>
				<ModalFooter>
					<Button color="secondary" onClick={toggleModal}>
						Cancel
					</Button>
				</ModalFooter>
			</Modal>
		</div>
	);
};

export default EditModal;

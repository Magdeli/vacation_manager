import React, { useState, useContext } from "react";
import { useKeycloak } from "react-keycloak";
import "./NavMenu.scss";
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
	Button
} from "reactstrap";
import NotificationList from "../NotificationList/NotificationList";
import { AuthContext } from "../../Contexts/AuthContext";

const NavMenu: React.FC = () => {
	const { currentUser, tokens, removeCurrentUser, removeTokens } = useContext(AuthContext);

	const [navigationDrawerIsOpen, setNavigationDrawerIsOpen] = useState(false);

	const toggleNavDrawer = () => setNavigationDrawerIsOpen(!navigationDrawerIsOpen);

	const [keycloak, initialized] = useKeycloak();
	let isAdmin = false;
	if (initialized) {
		if (keycloak.hasRealmRole("admin")) {
			isAdmin = true;
		}
	}

	if (!currentUser) {
		return <span />;
	}

	let adminSettingsBtn = isAdmin ? (
		<NavItem>
			<NavLink href="/admin">App settings</NavLink>
		</NavItem>
	) : (
		""
	);

	return (
		<div className="nav-menu-container">
			<Navbar color="light" light expand="md" className="nav-menu">
				<NavbarBrand href="/">Vacation Manager</NavbarBrand>
				<UncontrolledDropdown>
					<DropdownToggle nav caret>
						Notifications
					</DropdownToggle>
					<DropdownMenu>
						<DropdownItem>
							<NotificationList />
						</DropdownItem>
					</DropdownMenu>
				</UncontrolledDropdown>
				<NavbarToggler onClick={toggleNavDrawer} />
				<Collapse isOpen={navigationDrawerIsOpen} navbar>
					<Nav className="" navbar>
						<NavItem>
							<NavLink href="/profile">
								{currentUser.firstName + " " + currentUser.lastName}
							</NavLink>
						</NavItem>
						{adminSettingsBtn}
						<UncontrolledDropdown nav inNavbar>
							<DropdownToggle nav caret>
								Manage requests
							</DropdownToggle>
							<DropdownMenu right>
								<DropdownItem>
									<NavLink href={`/vacations/${currentUser.userId}`}>My vacations</NavLink>
								</DropdownItem>
								<DropdownItem>
									<NavLink href="/newrequest">Create new request</NavLink>
								</DropdownItem>
							</DropdownMenu>
						</UncontrolledDropdown>
						<Button
							onClick={() => {
								removeCurrentUser();
								removeTokens();
								keycloak.logout();
							}}
						>
							Logout
						</Button>
					</Nav>
				</Collapse>
			</Navbar>
		</div>
	);
};

export default NavMenu;

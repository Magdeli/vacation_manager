import React from "react";

import { useKeycloak } from "react-keycloak";
import { Route } from "react-router";

import { ForbiddenPage } from "./../../Pages";

const AuthorizedRoute: React.FC<{
	adminOnly?: boolean;
	exact?: boolean;
	path: string;
	component: any;
}> = ({ adminOnly, exact, path, component }) => {
	const [keycloak, initialized] = useKeycloak();

	if (initialized) {
		if (adminOnly && !keycloak.hasRealmRole("admin")) {
			return <Route exact={exact || false} path={path} component={ForbiddenPage} />;
		} else {
			return <Route exact={exact || false} path={path} component={component} />;
		}
	} else {
		return <span/>;
	}
};

export default AuthorizedRoute;

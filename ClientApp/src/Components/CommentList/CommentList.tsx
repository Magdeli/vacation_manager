import React, { useContext, useEffect, useState } from "react";
import { deleteComment, getRequestComments, postCommentToRequest } from "../../dataFetcher";
import { Button, ListGroup, ListGroupItem, Spinner } from "reactstrap";
import { CommentForm, EditComment } from "../../Components";
import "./CommentList.scss";
import { AuthContext } from "../../Contexts/AuthContext";

type Comment = {
	comment: {
		commentId: number;
		message: string;
		requestId: number;
		userId: number;
	};
	user: {
		name: string;
		profilePicture: string;
	};
};

const CommentList: React.FC<{ vacation: any; requestId: any }> = props => {
	const { currentUser } = useContext(AuthContext);
	const [comments, setComments] = useState<Array<Comment>>([]);
	const [commentForm, setCommentForm] = useState<Boolean>(false);
	const [loading, setLoading] = useState<Boolean>(false);
	const [commentInputValues, setCommentInputValues] = useState({
		userId: 0,
		requestId: props.requestId,
		message: ""
	});
	const [modal, setModal] = useState<boolean>(false);
	const [selectedComment, setSelectedComment] = useState<Comment>({
		comment: {
			commentId: 0,
			message: "",
			requestId: 0,
			userId: 0
		},
		user: {
			name: "",
			profilePicture: ""
		}
	});

	const onCommentInputChange = (e: React.FormEvent<HTMLInputElement>) => {
		setCommentInputValues({
			message: e.currentTarget.value,
			requestId: props.requestId,
			userId: currentUser.userId
		});
	};

	const handleCommentSubmit = (event: any) => {
		event.preventDefault();

		if (commentInputValues.message === "") {
			alert("You cannot submit an empty comment!");
		} else {
			setLoading(true);
			postCommentToRequest(props.requestId, commentInputValues)
				.then(data => {
					setComments(comments => [
						...comments,
						{
							comment: {
								commentId: data.commentId,
								message: data.message,
								requestId: data.requestId,
								userId: data.userId
							},
							user: {
								name: `${currentUser.firstName} ${currentUser.lastName}`,
								profilePicture: currentUser.profilePicture
							}
						}
					]);
					setLoading(false);
					alert("Success");
				})
				.catch(error => {
					console.log(error);
					setLoading(false);
				});
			event.target.reset();
			setCommentInputValues({
				userId: currentUser.userId,
				requestId: props.requestId,
				message: ""
			});
		}
	};

	const getData = () => {
		setLoading(true);
		getRequestComments(props.requestId)
			.then(data => {
				setComments(data);
				setLoading(false);
			})
			.catch(error => {
				console.log(error);
				setLoading(false);
			});
	};

	const toggleComment = () => {
		setCommentForm(!commentForm);
	};

	const sortByProperty = (property: any) => {
		return function(x: any, y: any) {
			return x[property] === y[property] ? 0 : x[property] < y[property] ? 1 : -1;
		};
	};

	const deleteThisComment = (event: any) => {
		const commentId = event.target.parentNode.parentNode.id;
		deleteComment(props.vacation.requestId, commentId)
			.then(res => console.log(res))
			.catch(err => console.error(err));
		setTimeout(() => {
			getData();
		}, 500);
	};

	const handleEdit = (event: any) => {
		const selectedId = event.target.parentNode.parentNode.id;
		// @ts-ignore
		const selectedComment: Comment = comments.find(
			comment => comment.comment.commentId == selectedId
		);
		setSelectedComment(selectedComment);
		toggleModal();
	};

	const toggleModal = () => {
		setModal(!modal);
	};

	let spinner = loading ? (
		<div className="text-center">
			<Spinner />
		</div>
	) : (
		""
	);

	let newCommentBtnText = commentForm ? "Close" : "Make a new comment";

	useEffect(() => {
		getData();
	}, []);

	return (
		<div className="comments">
			<h4>Comments</h4>
			<Button className="new-comment-button" onClick={toggleComment}>
				{newCommentBtnText}
			</Button>
			<CommentForm
				toggleForm={commentForm}
				commentInputValues={commentInputValues}
				onChange={onCommentInputChange}
				onSubmit={handleCommentSubmit}
			/>
			{spinner}
			{comments.sort(sortByProperty("commentId")).map(comment => (
				<div key={comment.comment.commentId} className="single-comment">
					<ListGroup style={{ marginTop: "1em" }}>
						<ListGroupItem id={comment.comment.commentId.toString()}>
							{comment.user.name}: {comment.comment.message}
							<br />
							<div className="comment-buttons">
								<Button className="edit-comment-button" onClick={handleEdit}>
									Edit
								</Button>
								<Button color="danger" onClick={deleteThisComment}>
									Delete
								</Button>
							</div>
						</ListGroupItem>
					</ListGroup>
				</div>
			))}

			<EditComment
				updateComments={getData}
				comment={selectedComment.comment}
				requestId={props.requestId}
				isOpen={modal}
				toggleModal={toggleModal}
			/>
		</div>
	);
};

export default CommentList;

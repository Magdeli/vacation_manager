import React, {useState} from 'react';
import {Button, Container, Form, FormGroup, Input, Label,} from 'reactstrap';
import {postUser} from "../../dataFetcher";

type InputValues = {
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    profilePicture: string
}

const NewUserForm: React.FC = () => {


    const [type,setType] = useState<String>("User");
    const [inputValues, setInputValues] = useState<InputValues>({
        firstName: "",
        lastName: "",
        password: "",
        email: "",
        profilePicture: "https://i.kym-cdn.com/entries/icons/original/000/016/546/hidethepainharold.jpg"
    });

    const onInputChange = (e: React.FormEvent<HTMLInputElement>) => {
        const { name, value } = e.currentTarget;
        setInputValues({ ...inputValues, [name]: value });
    };

    const handleFormSubmit = (event : any) => {
        event.preventDefault();
        sendData();
        setInputValues({
            firstName: "",
            lastName: "",
            password: "",
            email: "",
            profilePicture: "https://i.kym-cdn.com/entries/icons/original/000/016/546/hidethepainharold.jpg"
        });
        event.target.reset();
    };


    const handleChangeType = (event : any) => {
        setType(event.target.value);
    };


    const sendData = () => {
      const newUser = {
          email: inputValues.email,
          firstName: inputValues.firstName,
          isAdmin: type == "Admin",
          lastName: inputValues.lastName,
          profilePicture: inputValues.profilePicture,
      };

      postUser(newUser).then(data => {
          alert(`User ${inputValues.email} was successfully added`)
      }).catch(error => {
          console.log(error);
          alert(`Error adding user! Check the error logs for more info!`)
      })
    };

    return(
        <div className="form-div">
            <Container className="admin-form-container">
                <h1>Add new user</h1>
                <Form className="admin-new-user-form" onSubmit={handleFormSubmit}>
                    <FormGroup>
                        <Label>Username(Email):</Label>
                        <Input className="form-control" type="email" name="email" onChange={onInputChange}/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Password:</Label>
                        <Input className="form-control" type="text" name="password" onChange={onInputChange}/>
                    </FormGroup>
                    <FormGroup>
                        <Label>First name:</Label>
                        <Input className="form-control" type="text" name="firstName" onChange={onInputChange}/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Last name:</Label>
                        <Input className="form-control" type="text" name="lastName" onChange={onInputChange}/>
                    </FormGroup>
                    <FormGroup>
                        <label htmlFor="admin-new-user-form">Select type of user:</label>
                        <select className="form-control" id="user-privilege-select" onChange={handleChangeType}>
                            <option>User</option>
                            <option>Admin</option>
                        </select>
                    </FormGroup>
                    <Button type="submit" color="primary" value="Create user">
                        Create new user
                    </Button>
                </Form>
            </Container>
        </div>
    )
};

export default NewUserForm;
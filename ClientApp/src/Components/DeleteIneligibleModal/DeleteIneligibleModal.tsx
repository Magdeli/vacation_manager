import React from "react";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {deletePeriod} from "../../dataFetcher";

const DeleteIneligbleModal: React.FC<{
	toggleModal: any;
	isOpen: boolean;
	updateIneligiblePeriod: Function;
	id: any;
}> = ({ toggleModal, isOpen, updateIneligiblePeriod, id }) => {
	const handleOnClick = (event: any) => {
		event.preventDefault();
		sendData();
		setTimeout(() => {
			updateIneligiblePeriod();
		}, 300);
	};

	const sendData = () => {
		deletePeriod(id)
			.then(res => console.log(res))
			.catch(err => console.error(err));
	};

	return (
		<div className="modal-div">
			<Modal isOpen={isOpen} toggle={toggleModal}>
				<ModalHeader toggle={toggleModal}>Delete</ModalHeader>
				<ModalBody>
					<p>Delete this ineligible period?</p>
					<Button onClick={handleOnClick} color="danger">
						Delete
					</Button>
				</ModalBody>
				<ModalFooter>
					<Button color="secondary" onClick={toggleModal}>
						Cancel
					</Button>
				</ModalFooter>
			</Modal>
		</div>
	);
};

export default DeleteIneligbleModal;

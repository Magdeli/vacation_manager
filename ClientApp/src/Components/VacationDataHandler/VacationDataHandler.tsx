import React, {useState} from 'react';
import {Button, FormGroup, Label} from "reactstrap";
import {getAllRequests} from "../../dataFetcher";
import './VacationDataHandler.scss'

const VacationDataHandler: React.FC = () => {

    const [vacationData, setVacationData] = useState("");

    const handleInput = (e: any) => {
        setVacationData(e.target.value);
    };

    const handleSubmit = (e: any) => {
        e.preventDefault();
        if (validJson(vacationData)) {
            sendVacations();
        } else {
            alert("The data you submitted is not a valid JSON string, please provide a string in a JSON format");
        }
    };


    const getVacations = () => {
        getAllRequests().then(data => {
            console.log(JSON.stringify(data));
            alert("Data printed, please check your dev console!")
        }).catch(error => {
            console.log(error)
        });
    };

    const sendVacations = () => {
        alert("Vacation data sent!")
    };

    const validJson = (input: string) => {
        return /^[\],:{}\s]*$/.test(input.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''));
    };

    const prettyPrint = (event: any) => {
        const input = event.target.parentNode.parentNode[0];
        const ugly = input.value;
        const obj = JSON.parse(ugly);
        input.value = JSON.stringify(obj, undefined, 4);
    };

    return (
        <div className="vacation-data-handler">

            <Button onClick={() => {
                getVacations()
            }} className='get-data-button'>Get all vacation data as JSON</Button>

            <form onSubmit={handleSubmit}>
                <FormGroup>
                    <Label htmlFor="vacation-textarea">Please insert a valid JSON string</Label>
                    <textarea className="form-control vacation-json" id="vacation-textarea" rows={20}
                              onChange={handleInput}/>
                    <Button className='send-data-button' onClick={prettyPrint}>Convert to pretty JSON</Button>
                    <Button className='send-data-button'>Send new vacation data</Button>
                </FormGroup>
            </form>
        </div>
    )
};

export default VacationDataHandler;
import React, { useState } from "react";
import {
	Button,
	Form,
	FormGroup,
	Label,
	Input,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter
} from "reactstrap";
import { postPeriod } from "../../dataFetcher";

const IneligiblePeriodModal: React.FC<{
	toggleModal: any;
	isOpen: boolean;
	updateIneligiblePeriod: Function;
}> = ({ toggleModal, isOpen, updateIneligiblePeriod }) => {
	const [inputValues, setInputValues] = useState({});

	const handleModalSubmit = (event: any) => {
		event.preventDefault();
		sendData();
		setInputValues({});
	};

	const onInputChange = (e: React.FormEvent<HTMLInputElement>) => {
		const { name, value } = e.currentTarget;
		setInputValues({ ...inputValues, [name]: value });
	};

	const sendData = () => {
		postPeriod(inputValues)
			.then(data => {
				alert(`A new ineligible period was successfully updated`);
				updateIneligiblePeriod();
			})
			.catch(error => {
				console.log(error);
				alert(`Error creating new ineligible period`);
			});
	};

	return (
		<div className="modal-div">
			<Modal isOpen={isOpen} toggle={toggleModal}>
				<ModalHeader toggle={toggleModal}>Create new ineligible period</ModalHeader>
				<ModalBody>
					<Form onSubmit={handleModalSubmit}>
						<FormGroup>
							<Label>Start date:</Label>
							<Input
								className="form-control"
								type="date"
								name="periodStart"
								onChange={onInputChange}
							/>
						</FormGroup>
						<FormGroup>
							<Label>End date:</Label>
							<Input
								className="form-control"
								type="date"
								name="periodEnd"
								onChange={onInputChange}
							/>
						</FormGroup>
						<Button type="submit" color="primary">
							Create
						</Button>
					</Form>
				</ModalBody>
				<ModalFooter>
					<Button color="secondary" onClick={toggleModal}>
						Cancel
					</Button>
				</ModalFooter>
			</Modal>
		</div>
	);
};

export default IneligiblePeriodModal;

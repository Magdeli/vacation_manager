import React from "react";
import {Button, Container, Form, FormGroup, Input, Label} from "reactstrap";
import "./RequestForm.scss";

const RequestForm: React.FC<{ inputValues: Object; onChange: any; onSubmit: any }> = ({
	onChange,
	onSubmit
}) => {
	return (
        <div className='create-request-form'>
			<Container style={{ width: "35%" }}>
				<h1>New vacation request</h1>
				<Form onSubmit={onSubmit}>
					<FormGroup>
						<Label>Title</Label>
						<Input className="form-control" type="text" name="title" onChange={onChange} required />
					</FormGroup>
					<FormGroup>
						<Label>Start date</Label>
						<Input
							className="form-control"
							type="date"
							name="periodStart"
							onChange={onChange}
							required
						/>
						<Label>End date</Label>
						<Input
							className="form-control"
							type="date"
							name="periodEnd"
							onChange={onChange}
							required
						/>
					</FormGroup>
					<FormGroup>
						<Label>Initial comment</Label>
						<Input className="form-control" type="textarea" name="message" onChange={onChange} />
					</FormGroup>
                    <Button className='create-request-button' type="submit" color="primary" value="Create request">
						Create request
					</Button>
				</Form>
			</Container>
		</div>
	);
};

export default RequestForm;

import React from "react";
import {Button, Form, FormGroup, Input, Label} from "reactstrap";

const CommentForm: React.FC<{
	toggleForm: any;
	commentInputValues: Object;
	onChange: any;
	onSubmit: any;
}> = ({toggleForm, onChange, onSubmit}) => {
    if (toggleForm) {
		return (
			<div>
				<Form onSubmit={onSubmit}>
					<FormGroup>
						<Label>New comment</Label>
						<Input
							className="form-control"
							placeholder="Leave a comment here..."
							type="textarea"
							name="message"
							onChange={onChange}
						/>
					</FormGroup>
					<Button type="submit" color="primary">
						Submit
					</Button>
				</Form>
			</div>
		);
	}
	return null;
};

export default CommentForm;

import React from "react";
import {Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {patchUser} from "../../dataFetcher";

const ProfilePictureModal : React.FC<{toggleModal : any, isOpen: boolean, userId: number }> = ({toggleModal, isOpen, userId}) => {

    const validURL = (string : string) => {
        const pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
            '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return pattern.test(string);
    };

    const handleModalSubmit = (event : any) => {
        event.preventDefault();
        const url = event.target[0].value;
        if(validURL(url)){
            sendData(url);
        } else {
            event.target.reset();
            alert("Invalid URL! Please enter a valid URL and try again!");
        }
    };

    const sendData = (picUrl : string) => {
        patchUser({profilePicture: picUrl },userId).then(
            data => {
                alert(`Picture successfully updated`)
            }
        ).catch(error => {
            console.log(error);
        })
    };

    return(
        <div className="modal-div">
            <Modal isOpen={isOpen} toggle={toggleModal} className="edit-user-modal">
                <ModalHeader toggle={toggleModal}>Edit user details: </ModalHeader>
                <ModalBody>
                    <Form className="admin-edit-user-form" onSubmit={handleModalSubmit}>
                        <FormGroup>
                            <Label>Picture URL:</Label>
                            <Input className="form-control" type="text" name="picture-url"/>
                        </FormGroup>
                        <Button type="submit" color="primary" value="Update picture">
                            Update picture
                        </Button>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggleModal}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
};

export default ProfilePictureModal;
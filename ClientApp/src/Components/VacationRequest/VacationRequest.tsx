import React from "react";
import {Link} from "react-router-dom";
import {Card, CardText, CardTitle} from "reactstrap";
import "./VacationRequest.scss";
import moment from "moment";

const VacationRequest: React.FC<{
	id: string;
	title: string;
	periodStart: string;
	periodEnd: string;
}> = ({ id, title, periodStart, periodEnd }) => {


    return (
		<div className="vacation-request">
			<Card body>
				<CardTitle>
					<h3 className='request-title'>Request Title: {title}</h3>
					<h4 className='request dates'>
                        <p>Date start: {moment(periodStart).format("MMMM Do YYYY")}</p>
						<p>Date end: {moment(periodEnd).format('MMMM Do YYYY')}</p>
					</h4>
				</CardTitle>
				<CardText>
					<Link to={`/vacation/${id}`}>View request</Link>
				</CardText>
			</Card>
		</div>
	);
};

export default VacationRequest;

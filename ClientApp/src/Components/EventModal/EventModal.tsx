import React from "react";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader,} from "reactstrap";
import {Link} from "react-router-dom";

const EventModal: React.FC<{
	userName: any;
	userId: any;
	requestId: any;
	title: any;
	toggleModal: any;
	isOpen: boolean;
}> = ({ userName, userId, requestId, title, toggleModal, isOpen }) => {
	return (
		<div className="modal-div">
			<Modal isOpen={isOpen} toggle={toggleModal}>
				<ModalHeader toggle={toggleModal}>Event overview</ModalHeader>
				<ModalBody>
					<h4>Event title: </h4>
					<p>
						<Link to={`vacation/${requestId}`}>{title}</Link>
					</p>
					<h4>Name: </h4>
					<p>
						<Link to={`vacations/${userId}`}>{userName}</Link>
					</p>
				</ModalBody>
				<ModalFooter>
					<Button color="secondary" onClick={toggleModal}>
						Cancel
					</Button>
				</ModalFooter>
			</Modal>
		</div>
	);
};

export default EventModal;

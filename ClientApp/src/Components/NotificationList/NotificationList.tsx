import React, { useEffect, useState } from "react";
import { getAllNotifications } from "../../dataFetcher";
import { ListGroup, ListGroupItem } from "reactstrap";
import ReactHtmlParser from "react-html-parser";

import "./NotificationList.scss";

const NotificationList: React.FC = () => {
	const [notifications, setNotifications] = useState([]);

	const getNotifications = () => {
		getAllNotifications()
			.then(data => {
				setNotifications(data);
			})
			.catch(error => {
				console.log(error);
			});
	};

	useEffect(() => {
		getNotifications();
	}, []);

	return (
		<div className="notification-list">
			{notifications.map(notif => {
				return <NotificationListItem key={""} Notification={notif} />;
			})}
		</div>
	);
};

type User = {
	userId: number;
	email: string;
	isAdmin: boolean;
	firstName: string;
	lastName: string;
	profilePicture: string;
};

type Notification = {
	notificationtype: string;
	message: string;
	notifierUser: User;
};

const NotificationListItem: React.FC<{ Notification: Notification }> = ({ Notification }) => {
	return (
		<div className="notification-list-item">
			<img
				className="notification-image"
				src={Notification.notifierUser.profilePicture}
				alt={
					"Profile picture of " +
					Notification.notifierUser.firstName +
					" " +
					Notification.notifierUser.lastName
				}
			/>
			<p className="notification-text">
				{ReactHtmlParser(Notification.message)}
				<br />
			</p>
		</div>
	);
};

export default NotificationList;

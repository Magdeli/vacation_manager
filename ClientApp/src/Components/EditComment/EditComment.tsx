import React, { useState } from "react";
import {
	Button,
	Form,
	FormGroup,
	Label,
	Input,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter
} from "reactstrap";
import { editComment } from "../../dataFetcher";

const EditComment: React.FC<{
	toggleModal: any;
	isOpen: boolean;
	requestId: any;
	comment: any;
	updateComments: Function;
}> = ({ requestId, toggleModal, isOpen, comment, updateComments }) => {
	const [inputValues, setInputValues] = useState({});

	const handleModalSubmit = (event: any) => {
		event.preventDefault();
		sendData();
		setInputValues({});
		setTimeout(() => {
			updateComments();
		}, 1000);
	};

	const onInputChange = (e: React.FormEvent<HTMLInputElement>) => {
		const { name, value } = e.currentTarget;
		setInputValues({ ...inputValues, [name]: value });
	};

	const sendData = () => {
		editComment(requestId, comment.commentId, inputValues)
			.then(data => {
				console.log(data);
				alert(`Comment was successfully updated`);
			})
			.catch(error => {
				console.log(error);
				alert(`Error updating comment!`);
			});
	};

	return (
		<div className="modal-div">
			<Modal isOpen={isOpen} toggle={toggleModal}>
				<ModalHeader toggle={toggleModal}>Edit comment</ModalHeader>
				<ModalBody>
					<Form onSubmit={handleModalSubmit}>
						<FormGroup>
							<Label>Comment:</Label>
							<Input
								className="form-control"
								type="textarea"
								name="message"
								defaultValue={comment.message}
								onChange={onInputChange}
							/>
						</FormGroup>
						<Button type="submit" color="primary">
							Update comment
						</Button>
					</Form>
				</ModalBody>
				<ModalFooter>
					<Button color="secondary" onClick={toggleModal}>
						Cancel
					</Button>
				</ModalFooter>
			</Modal>
		</div>
	);
};

export default EditComment;

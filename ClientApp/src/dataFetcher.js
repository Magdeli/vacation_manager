//const apiUrl = "https://vacationmanagerapp.azurewebsites.net/api/";

const apiUrl = "http://localhost:7071/api/";

const requestUrl = apiUrl + "request/";
const userUrl = apiUrl + "user/";
const periodsUrl = apiUrl + "ineligible/";
const notificationUrl = apiUrl + "notification/";

//----------------
//Generic fetchers
//----------------

const singleItemFetch = url => {
	return fetch(`${url}`, {
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		//mode: "no-cors", // no-cors, *cors, same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		credentials: "same-origin", // include, *same-origin, omit
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${sessionStorage.getItem("react-token")}`
		},
		redirect: "follow", // manual, *follow, error
		referrer: "no-referrer" // no-referrer, *client
	}).then(response => response.json());
};

const singleItemPost = (url, data) => {
	return fetch(`${url}`, {
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		//mode: "same-origin", // no-cors, *cors, same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		credentials: "same-origin", // include, *same-origin, omit
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${sessionStorage.getItem("react-token")}`
		},
		redirect: "follow", // manual, *follow, error
		referrer: "no-referrer", // no-referrer, *client
		body: JSON.stringify(data) // body data type must match "Content-Type" header
	}).then(response => response.json());
};

const singleItemDelete = url => {
	return fetch(`${url}`, {
		method: "DELETE", // *GET, POST, PUT, DELETE, etc.
		//mode: "same-origin", // no-cors, *cors, same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		credentials: "same-origin", // include, *same-origin, omit
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${sessionStorage.getItem("react-token")}`
		},
		redirect: "follow", // manual, *follow, error
		referrer: "no-referrer" // no-referrer, *client
	}).then(response => response.json());
};

const singleItemPatch = (url, data) => {
	return fetch(`${url}`, {
		method: "PATCH", // *GET, POST, PUT, DELETE, etc.
		//mode: "same-origin", // no-cors, *cors, same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		credentials: "same-origin", // include, *same-origin, omit
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${sessionStorage.getItem("react-token")}`
		},
		redirect: "follow", // manual, *follow, error
		referrer: "no-referrer", // no-referrer, *client
		body: JSON.stringify(data) // body data type must match "Content-Type" header
	}).then(response => response.json());
};

//----------------
// Vacation Requests
//----------------

export const getSingleRequest = id => {
	return singleItemFetch(requestUrl + id);
};

export const getAllRequests = () => {
	return singleItemFetch(requestUrl);
};

export const getUserRequests = userId => {
	return singleItemFetch(userUrl + `${userId}/requests`);
};

export const postVacationRequest = data => {
	return singleItemPost(requestUrl, data);
};
export const patchRequest = (data, id) => {
	return singleItemPatch(requestUrl + `${id}`, data);
};

export const deleteRequest = id => {
	return singleItemDelete(requestUrl + id);
};
//----------------
//User requests
//----------------

export const getLoggedInUser = () => {
	return singleItemFetch(userUrl);
};

export const getSingleUser = id => {
	return singleItemFetch(userUrl + id);
};

export const getAllUsers = () => {
	return singleItemFetch(userUrl + "all");
};

export const postUser = data => {
	return singleItemPost(userUrl, data);
};

export const patchUser = (data, id) => {
	return singleItemPatch(userUrl + `${id}`, data);
};

export const deleteUser = id => {
	return singleItemDelete(userUrl + id);
};

export const updatePassword = (data, id) => {
	return singleItemPost(userUrl + `${id}`, data);
};

//----------------
//Comment requests
//----------------

export const getRequestComments = requestId => {
	return singleItemFetch(requestUrl + `${requestId}/comments`);
};

export const getSingleRequestComment = (requestId, commentsId) => {
	return singleItemFetch(requestUrl + `${requestId}/comments/${commentsId}`);
};

export const postCommentToRequest = (requestId, data) => {
	return singleItemPost(requestUrl + `${requestId}/comments`, data);
};

export const editComment = (requestId, commentsId, data) => {
	return singleItemPatch(requestUrl + `${requestId}/comments/${commentsId}`, data);
};

export const deleteComment = (requestId, commentsId) => {
	return singleItemDelete(requestUrl + `${requestId}/comments/${commentsId}`);
};

//---------------------------
//Ineligible periods requests
//---------------------------

export const getPeriods = () => {
	return singleItemFetch(periodsUrl);
};

export const getSinglePeriod = id => {
	return singleItemFetch(periodsUrl + `${id}`);
};

export const postPeriod = data => {
	return singleItemPost(periodsUrl, data);
};

export const patchPeriod = (data, id) => {
	return singleItemPatch(periodsUrl + `${id}`, data);
};

export const deletePeriod = id => {
	return singleItemDelete(periodsUrl + id);
};

//-------------
//Notifications
//-------------

export const getAllNotifications = () => {
	return singleItemFetch(notificationUrl);
};

export const postNotification = data => {
	return singleItemPost(notificationUrl, data);
};

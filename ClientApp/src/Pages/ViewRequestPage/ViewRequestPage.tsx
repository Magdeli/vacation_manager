import React, { FormEvent, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import "./ViewRequestPage.scss";
import { deleteRequest, getSingleRequest, getSingleUser, patchRequest } from "../../dataFetcher";
import {
	Button,
	Container,
	Form,
	FormGroup,
	Input,
	Label,
	ListGroup,
	ListGroupItem,
	Spinner
} from "reactstrap";
import CommentList from "../../Components/CommentList/CommentList";
import { useKeycloak } from "react-keycloak";

const ViewRequestPage: React.FC = (props: any) => {
	interface IVacationRequest {
		periodEnd: string;
		periodStart: string;
		requestId: number;
		title: string;
		userId: string;
		vacationRequestStatus: string;
		vacationRequestStatusId: number;
	}

	const [keycloak, initialized] = useKeycloak();
	let isAdmin = false;
	if (initialized) {
		if (keycloak.hasRealmRole("admin")) {
			isAdmin = true;
		}
	}
	const [vacation, setVacation] = useState<IVacationRequest>(Object);
	const [inputValues, setInputValues] = useState({});
	const [loading, setLoading] = useState<Boolean>(false);
	const handleEdit = (event: FormEvent) => {
		setLoading(true);
		event.preventDefault();
		patchRequest(inputValues, props.match.params.id)
			.then(data => {
				setVacation(data);
				setLoading(false);
				alert("Successful update");
			})
			.catch(error => {
				console.log(error);
				setLoading(false);
			});
	};

	const redirect = () => {
		props.history.push(`/`);
	};

	const handleOnClickDelete = (event: any) => {
		event.preventDefault();
		if (window.confirm("Are you sure you want to delete this request?")) {
			deleteData();
			setTimeout(() => {
				alert("You have successfully deleted this request");
				redirect();
			}, 300);
		}
	};

	const deleteData = () => {
		deleteRequest(props.match.params.id)
			.then(res => console.log(res))
			.catch(err => console.error(err));
	};

	const onInputChange = (e: React.FormEvent<HTMLInputElement>) => {
		const { name, value } = e.currentTarget;
		setInputValues({ ...inputValues, [name]: value });
	};

	const getData = () => {
		setLoading(true);
		getSingleRequest(props.match.params.id)
			.then(data => {
				setVacation(data.vacationRequest);
				setLoading(false);
			})
			.catch(error => {
				console.log(error);
				setLoading(false);
			});
	};

	const changeStatus = (statusCode: number) => {
		const vacationtatus = {
			vacationRequestStatusId: statusCode
		};

		patchRequest(vacationtatus, props.match.params.id)
			.then(data => {
				setVacation(data);
				setLoading(false);
			})
			.catch(error => {
				console.log(error);
				setLoading(false);
			});
	};

	useEffect(() => {
		getData();
	}, []);

	let spinner = loading ? (
		<div className="text-center">
			<Spinner />
		</div>
	) : (
		""
	);
	let statusEditingBtns = isAdmin ? (
		<div className="status-buttons">
			<Button
				color="success"
				onClick={() => {
					changeStatus(2);
				}}
			>
				Approve
			</Button>
			<Button
				color="danger"
				onClick={() => {
					changeStatus(3);
				}}
			>
				Revoke
			</Button>
		</div>
	) : (
		""
	);

	let deleteRequestBtn = isAdmin ? (
		<Button outline color="danger" onClick={handleOnClickDelete}>
			Delete request
		</Button>
	) : (
		""
	);

	return (
		<div className="view-request-wrapper">
			<Container style={{ paddingBottom: "25px" }}>
				<div className="view-request">
					<h1>Vacation request</h1>
					{spinner}
					{deleteRequestBtn}
					<div className="request-form-container">
						<Form className="request-form">
							<FormGroup>
								<Label>Title:</Label>
								<Input
									type="text"
									name="title"
									defaultValue={vacation.title}
									onChange={onInputChange}
									readOnly={vacation.vacationRequestStatusId === 2 && !isAdmin}
								/>
							</FormGroup>
							<FormGroup>
								<Label>Period start:</Label>
								<Input
									type="text"
									name="periodStart"
									defaultValue={vacation.periodStart}
									onChange={onInputChange}
									readOnly={vacation.vacationRequestStatusId === 2 && !isAdmin}
								/>
							</FormGroup>
							<FormGroup>
								<Label>Period end:</Label>
								<Input
									type="text"
									name="periodEnd"
									defaultValue={vacation.periodEnd}
									onChange={onInputChange}
									readOnly={vacation.vacationRequestStatusId === 2 && !isAdmin}
								/>
							</FormGroup>
							<Button
								type="submit"
								color="primary"
								onClick={handleEdit}
								disabled={vacation.vacationRequestStatusId === 2 && !isAdmin}
							>
								Save changes
							</Button>

							<ListGroup className="status-container" style={{ marginTop: "1em" }}>
								<ListGroupItem>
									{" "}
									Status:{" "}
									{(() => {
										switch (vacation.vacationRequestStatusId) {
											case 2:
												return "Approved";
											case 3:
												return "Denied";
											case 1:
												return "Pending";
										}
									})()}
									{statusEditingBtns}
								</ListGroupItem>
								<ListGroupItem>
									<NavLink to={{ pathname: `/vacations/${vacation.userId}` }} className="">
										Go to request owner's history
									</NavLink>
								</ListGroupItem>
							</ListGroup>
						</Form>
					</div>
					<CommentList requestId={props.match.params.id} vacation={vacation} />
				</div>
			</Container>
		</div>
	);
};

export default ViewRequestPage;

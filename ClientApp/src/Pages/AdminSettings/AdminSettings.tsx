import React, { useState } from "react";
import "./AdminSettings.scss";
import {
	Button,
	Col,
	FormGroup,
	Input,
	Label,
	ListGroup,
	ListGroupItem,
	Nav,
	NavItem,
	NavLink,
	Row,
	TabContent,
	TabPane
} from "reactstrap";

import classnames from "classnames";
import { getAllUsers } from "../../dataFetcher";
import { EditModal, NewUserForm, VacationDataHandler } from "./../../Components";

type User = {
	userId: number;
	email: string;
	isAdmin: boolean;
	firstName: string;
	lastName: string;
	profilePicture: string;
};

const AdminSettings: React.FC = () => {
	const [activeTab, setActiveTab] = useState<string>("1");
	const [modal, setModal] = useState<boolean>(false);
	const [selectedUser, setSelectedUser] = useState<User>({
		email: "",
		firstName: "",
		isAdmin: false,
		lastName: "",
		userId: 0,
		profilePicture: ""
	});
	const [userList, setUserList] = useState<Array<User>>([]);

	const getData = () => {
		getAllUsers()
			.then(data => {
				setUserList(data);
			})
			.catch(error => {
				console.log(error);
			});
	};

	const toggle = (tab: string) => {
		if (activeTab !== tab) setActiveTab(tab);
	};

	const toggleModal = () => setModal(!modal);

	const handleUpdate: Function = (event: any) => {
		const selectedId = parseInt(event.target.parentElement.parentElement.id);
		const selectedUser = userList.find(user => user.userId === selectedId);
		// @ts-ignore
		//Suppressing the strict null check, will handle it in request functions
		setSelectedUser(selectedUser);
		toggleModal();
	};

	let renderedUsers: Array<JSX.Element> = [];
	const normalUser: string = "info";
	const adminUser: string = "danger";

	if (userList.length > 0) {
		renderedUsers = userList.map(user => (
			<ListGroupItem
				key={user.userId}
				id={`${user.userId}`}
				color={user.isAdmin ? adminUser : normalUser}
			>
				<div className="user-text">
					<p>
						{user.firstName} {user.lastName}
					</p>
					<p>{user.email}</p>
				</div>
				<div className="update-button-container">
					<Button
						className="update-button"
						color="primary"
						onClick={e => {
							handleUpdate(e);
						}}
					>
						Update
					</Button>
				</div>
			</ListGroupItem>
		));
	}

	return (
		<div className="content-container">
			<Nav tabs>
				<NavItem>
					<NavLink
						className={classnames({ active: activeTab === "1" })}
						onClick={() => {
							toggle("1");
						}}
					>
						Create New User
					</NavLink>
				</NavItem>
				<NavItem>
					<NavLink
						className={classnames({ active: activeTab === "2" })}
						onClick={() => {
							toggle("2");
						}}
					>
						User List
					</NavLink>
				</NavItem>
				<NavItem>
					<NavLink
						className={classnames({ active: activeTab === "3" })}
						onClick={() => {
							toggle("3");
						}}
					>
						Set period of vacation dates
					</NavLink>
				</NavItem>
				<NavItem>
					<NavLink
						className={classnames({ active: activeTab === "4" })}
						onClick={() => {
							toggle("4");
						}}
					>
						Get vacation data
					</NavLink>
				</NavItem>
			</Nav>

			<TabContent activeTab={activeTab}>
				<TabPane tabId="1">
					<Row>
						<Col sm="12">
							<NewUserForm />
						</Col>
					</Row>
				</TabPane>

				<TabPane tabId="2">
					<Row>
						<Col sm="12">
							<Button
								onClick={() => {
									getData();
								}}
							>
								Fetch users
							</Button>
							<ListGroup>{renderedUsers}</ListGroup>
							<EditModal User={selectedUser} isOpen={modal} toggleModal={toggleModal} />
						</Col>
					</Row>
				</TabPane>

				<TabPane tabId="3">
					<Row>
						<Col sm="12">
							<form>
								<FormGroup>
									<Label>Specify max number of vacation days:</Label>
									<Input className="form-control" type="number" name="lastName" />
								</FormGroup>
							</form>
						</Col>
					</Row>
				</TabPane>

				<TabPane tabId="4">
					<Row>
						<Col sm="12">
							<VacationDataHandler />
						</Col>
					</Row>
				</TabPane>
			</TabContent>
		</div>
	);
};
export default AdminSettings;

import AdminSettings from "./AdminSettings/AdminSettings";
import CalenderDashboard from "./CalenderDashboard/CalenderDashboard";
import ForbiddenPage from "./ForbiddenPage/ForbiddenPage";
import MyVacations from "./MyVacations/MyVacations";
import NewRequestForm from "./NewRequestForm/NewRequestForm";
import UserProfile from "./UserProfile/UserProfile";
import ViewRequestPage from "./ViewRequestPage/ViewRequestPage";

export {
	AdminSettings,
	CalenderDashboard,
	ForbiddenPage,
	MyVacations,
	NewRequestForm,
	UserProfile,
	ViewRequestPage
};

import React, {useContext, useEffect, useState} from "react";
import { NavLink } from "react-router-dom";
import { getSingleUser, getLoggedInUser } from "../../dataFetcher";
import { Container } from "reactstrap";
import "./UserProfile.scss";
import ProfilePictureModal from "../../Components/ProfilePictureModal/ProfilePictureModal";
import {AuthContext} from "../../Contexts/AuthContext";

type User = {
	userId: number;
	email: string;
	isAdmin: boolean;
	firstName: string;
	lastName: string;
	profilePicture: string;
};

const UserProfile: React.FC = (props: any) => {
	const [userDetails, setUserDetails] = useState<User>({
		email: "",
		firstName: "",
		isAdmin: false,
		lastName: "",
		userId: 0,
		profilePicture: ""
	});

	const [modal, setModal] = useState<boolean>(false);
	const toggleModal = () => setModal(!modal);

	const getData = () => {
		getLoggedInUser()
			.then(data => {
				setUserDetails(data);
			})
			.catch(error => {
				console.log(error);
			});
	};

	const updatePicture = () => {
		toggleModal();
	};

	useEffect(() => {
		setTimeout(() => {
			getData();
		}, 10);
	}, []);

	return (
		<div className="user-profile">
			<Container className="user-profile-container">
				<div className="profile-info">
					<img src={userDetails.profilePicture} alt="user-profile-picture" />
					<a href="#" onClick={updatePicture} className="">
						Change profile picture
					</a>
					<h2>
						{userDetails.firstName} {userDetails.lastName}
					</h2>
					<h3>{userDetails.email}</h3>
				</div>
				<div className="profile-links">
					<ul>
						<li>
							<NavLink to={{ pathname: "/" }} className="">
								Change password
							</NavLink>
						</li>
						<li>
							<NavLink to={{ pathname: "/" }} className="">
								Add two-factor authentication
							</NavLink>
						</li>
					</ul>
					<div>
						<ul>
							<li>
								<NavLink to={{ pathname: `/vacations/${userDetails.userId}` }} className="">
									My vacations
								</NavLink>
							</li>
							<li>
								<NavLink to={{ pathname: "/" }} className="">
									View calendar
								</NavLink>
							</li>
						</ul>
					</div>
				</div>
			</Container>
			<ProfilePictureModal
				isOpen={modal}
				toggleModal={toggleModal}
				userId={userDetails.userId}
			/>
		</div>
	);
};

export default UserProfile;

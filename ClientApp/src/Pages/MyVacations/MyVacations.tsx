import React, { useEffect, useState } from "react";
import { VacationRequest } from "../../Components";
import { Link } from "react-router-dom";
import { getUserRequests, getSingleUser } from "../../dataFetcher";
import { Button, Container } from "reactstrap";
import "./MyVacations.scss";

const MyVacations: React.FC = (props: any) => {
	const [vacations, setVacations] = useState<Array<any>>([]);
	const [userDetails, setUserDetails] = useState<any>({
		email: "",
		firstName: "",
		isAdmin: false,
		lastName: "",
		userId: 0,
		profilePicture: ""
	});

	const getData = () => {
		getUserRequests(props.match.params.id)
			.then(data => {
				setVacations(data);
			})
			.catch(error => {
				console.log(error);
			});

		getSingleUser(props.match.params.id)
			.then(data => {
				console.log(data);
				setUserDetails(data);
			})
			.catch(error => {
				console.log(error);
			});
	};

	useEffect(() => {
		getData();
	}, []);

	return (
		<div>
			<Container className="text-center">
				<div className="my-vacations">
					<div className="vacation-days">
						<h4>
							{userDetails.firstName} {userDetails.lastName}
						</h4>
						<Link to={`/newrequest`}>
							<Button color="primary">New Request</Button>
						</Link>
					</div>
					<div className="collection-list-div" style={{ marginTop: "2em" }}>
						{vacations.map(vacation => (
							<VacationRequest
								key={vacation.requestId}
								id={vacation.requestId}
								title={vacation.title}
								periodStart={vacation.periodStart}
								periodEnd={vacation.periodEnd}
							/>
						))}
					</div>
				</div>
			</Container>
		</div>
	);
};

export default MyVacations;

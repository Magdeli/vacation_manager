import React from "react";

const ForbiddenPage: React.FC<{ history: any }> = ({ history }) => (
	<>
		<h1>403 - Forbidden</h1>
		<p>The page you're trying to access is not available for you. </p>
		<p>If you think this is an error, please contact the IT department of your company.</p>
		<p>
			<a href={"/"}>Go back to the dashboard</a>
		</p>
	</>
);

export default ForbiddenPage;

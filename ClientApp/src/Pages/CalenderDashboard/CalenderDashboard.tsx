import React, { useEffect, useState, useContext } from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import moment from "moment";
import { getAllRequests, getPeriods, getLoggedInUser, getSingleUser } from "../../dataFetcher";
import { Button, Container } from "reactstrap";
import { DeleteIneligbleModal, EventModal, IneligiblePeriodModal } from "../../Components";
import { Link } from "react-router-dom";
import { useKeycloak } from "react-keycloak";
import "./CalenderDashboard.scss";
import { AuthContext } from "../../Contexts/AuthContext";

const CalendarDashboard: React.FC = () => {
	const { currentUser, tokens } = useContext(AuthContext);
	const [vacations, setVacations] = useState<Array<any>>([]);
	const [ineligblePeriods, setIneligblePeriods] = useState<Array<any>>([]);
	const [selectedEvent, setSelectedEvent] = useState<any>({});
	const [selectedUser, setSelectedUser] = useState<any>({});
	const [selectedPeriod, setSelectedPeriod] = useState<any>({});
	const [modal, setModal] = useState<boolean>(false);
	const [modalIneligible, setModalIneligible] = useState<boolean>(false);
	const [modalDeleteIneligible, setModalDeleteIneligible] = useState<boolean>(false);

	const localizer = momentLocalizer(moment);
	let allEvents = Array<any>();
	const [keycloak, initialized] = useKeycloak();
	let isAdmin = false;
	if (initialized) {
		if (keycloak.hasRealmRole("admin")) {
			isAdmin = true;
		}
	}

	const getData = () => {
		getAllRequests()
			.then(data => {
				setVacations(data);
			})
			.catch(error => {
				console.log(error);
			});

		getPeriods()
			.then(data => {
				setIneligblePeriods(data);
			})
			.catch(error => {
				console.log(error);
			});
	};

	const getUserName = (userId: number) => {
		getSingleUser(userId)
			.then(data => {
				setSelectedUser(data);
			})
			.catch(error => {
				console.log(error);
			});
	};

	const toggleModal = () => {
		setModal(!modal);
	};

	const toggleIneligibleModal = () => setModalIneligible(!modalIneligible);

	const toggleModalDeleteIneligible = () => setModalDeleteIneligible(!modalDeleteIneligible);

	useEffect(() => {
		getData();
	}, []);

	const handleOnSelectEvent = (id: any) => {
		const selectedEvent = vacations.find(vacation => vacation.requestId === id);
		setSelectedEvent(selectedEvent);
		getUserName(selectedEvent.userId);
		toggleModal();
	};

	const handleOnSelectEventIneligible = (id: any) => {
		const selectedIneligiblePeriod = events().find(event => event.ineligiblePeriodId === id);
		setSelectedPeriod(selectedIneligiblePeriod);
		toggleModalDeleteIneligible();
	};

	const events = () => {
		const ineligbleWithTitle = ineligblePeriods.map(ineligblePeriod => ({
			admin: ineligblePeriod.admin,
			adminId: ineligblePeriod.adminId,
			ineligiblePeriodId: ineligblePeriod.ineligiblePeriodId,
			periodStart: ineligblePeriod.periodStart,
			periodEnd: ineligblePeriod.periodEnd,
			title: "Ineligble period"
		}));
		allEvents = vacations.concat(ineligbleWithTitle);
		return allEvents.map(vacation => ({
			requestId: vacation.requestId,
			ineligiblePeriodId: vacation.ineligiblePeriodId,
			userId: vacation.userId,
			adminId: vacation.adminId,
			title: vacation.title,
			start: vacation.periodStart,
			end: vacation.periodEnd,
			requestStatus: vacation.vacationRequestStatus
		}));
	};

	let newIneligiblePeriodBtn = isAdmin ? (
		<Button className="new-period-button" onClick={toggleIneligibleModal}>
			Create new ineligible period
		</Button>
	) : (
		""
	);
	return (
		<div className="calendar">
			<Container>
				<div className="request-period-buttons">
					<Link to={`/newrequest`}>
						<Button className="new-request-button">New Request</Button>
					</Link>
					{newIneligiblePeriodBtn}
				</div>
				<IneligiblePeriodModal
					updateIneligiblePeriod={getData}
					isOpen={modalIneligible}
					toggleModal={toggleIneligibleModal}
				/>
				<DeleteIneligbleModal
					id={selectedPeriod.ineligiblePeriodId}
					updateIneligiblePeriod={getData}
					isOpen={modalDeleteIneligible}
					toggleModal={toggleModalDeleteIneligible}
				/>

				<Calendar
					tooltipAccessor={"title"}
					popup={true}
					onSelectEvent={event => {
						if (event.title != "Ineligble period") {
							handleOnSelectEvent(event.requestId);
						} else if (isAdmin) {
							handleOnSelectEventIneligible(event.ineligiblePeriodId);
						}
					}}
					events={events()}
					localizer={localizer}
					views={{ month: true }}
					eventPropGetter={event => {
						let newStyle = {
							backgroundColor: "#114b5f",
							color: "white"
						};
						if (event.title === "Ineligble period") {
							newStyle.backgroundColor = "#e84118";
						}
						if (event.userId === currentUser.userId) {
							newStyle.backgroundColor = "#009432";
						}
						return {
							style: newStyle
						};
					}}
				/>

				<div className="request-markers">
					<p>
						Your vacation requests: <span className="your-vacations" />
					</p>
					<p>
						Others: <span className="other-vacations" />
					</p>
					<p>
						Ineligble period: <span className="ineligble-periods" />{" "}
					</p>
				</div>
			</Container>
			<EventModal
				userName={`${selectedUser.firstName} ${selectedUser.lastName}`}
				userId={selectedEvent.userId}
				requestId={selectedEvent.requestId}
				title={selectedEvent.title}
				isOpen={modal}
				toggleModal={toggleModal}
			/>
		</div>
	);
};

export default CalendarDashboard;

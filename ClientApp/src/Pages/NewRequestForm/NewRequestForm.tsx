import React, { FormEvent, useState, useContext } from "react";
import { RequestForm } from "../../Components";
import { postCommentToRequest, postVacationRequest } from "../../dataFetcher";
import "./NewRequestForm.scss";
import { AuthContext } from "../../Contexts/AuthContext";

const NewRequestForm: React.FC = (props: any) => {
	const { currentUser } = useContext(AuthContext);
	const [inputValues, setInputValues] = useState({
		title: "",
		periodStart: "",
		periodEnd: "",
		message: ""
	});

	const redirect = (id: any) => {
		props.history.push(`/vacation/${id}`);
	};

	const handlePostComment = (requestId: any) => {
		const comment = {
			userId: currentUser.userId,
			requestId: requestId,
			message: inputValues.message
		};
		if (inputValues.message !== "") {
			postCommentToRequest(requestId, comment)
				.then(data => {
					redirect(data.requestId);
				})
				.catch(error => {
					console.log(error);
					alert("An error has occured");
				});
		}
		setTimeout(() => {
			redirect(requestId);
		}, 500);
	};

	const handleSubmit = (event: FormEvent) => {
		event.preventDefault();
		const request = {
			userId: currentUser.userId,
			title: inputValues.title,
			periodStart: inputValues.periodStart,
			periodEnd: inputValues.periodEnd,
			message: inputValues.message
		};
		postVacationRequest(request)
			.then(data => {
				handlePostComment(data.requestId);
			})
			.catch(error => {
				console.log(error);
				alert("An error has occured");
			});
	};

	const onInputChange = (e: React.FormEvent<HTMLInputElement>) => {
		const { name, value } = e.currentTarget;

		setInputValues({ ...inputValues, [name]: value });
	};

	return <RequestForm inputValues={inputValues} onChange={onInputChange} onSubmit={handleSubmit} />;
};

export default NewRequestForm;
